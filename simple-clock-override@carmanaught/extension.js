// Credit: stuartlangride for ideas from the Clock Override extension and ebassi
// for ideas from the Word Clock extension.

const GLib  = imports.gi.GLib;
const Shell = imports.gi.Shell;

const Main  = imports.ui.main;

let dateMenu = null;
let clockSignal = null;

function updateClock() {
    let timeDate = new Date()
    let dateFormat = Shell.util_translate_time_string(N_("%a, %b %-e, %l:%M %p"))
    dateMenu._clockDisplay.text = timeDate.toLocaleFormat(dateFormat);
}

function init() {
    let statusArea = Main.panel._statusArea;
    if (!statusArea)
        statusArea = Main.panel.statusArea;

    if (!statusArea || !statusArea.dateMenu) {
        log("Can't find either the statusArea or dateMenu.")
        return
    } else {
        dateMenu = statusArea.dateMenu;
    }
}

function enable () {
    if (!dateMenu)
        return

    if (!clockSignal)
        dateMenu._clock.disconnect(clockSignal);

    clockSignal = dateMenu._clock.connect('notify::clock', () => { updateClock(); });
    updateClock();
}

function disable () {
    if (!dateMenu)
        return

    if (!clockSignal) {
        dateMenu._clock.disconnect(clockSignal);
        clockSignal = null;
    }

    dateMenu._clockDisplay.text = dateMenu._clock.clock;
}