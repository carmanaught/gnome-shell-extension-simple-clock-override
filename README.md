# Simple Clock Override

Gnome Shell extension to override the clock text. This is just a small and limited extension to override the clock with a specific preferred clock format, without the need to have a more comprehensive and customizable extension.

The format can be modified directly from the code, but the valid time format codes / information are not provided here and must be sought elsewhere.
